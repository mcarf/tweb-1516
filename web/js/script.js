var pizzFunc =  {
  init: function() {
    $("#login").submit(pizzFunc.login);
    $("#logout").click(pizzFunc.logout);
    $("#prenotazione").submit(pizzFunc.insPren);
    priceRegex = /^\d+(\.\d{1,2})?$/;
  },
  login: function(e) {
    e.preventDefault();
    var user = {
      id: $("#user").val(),
      password: $("#password").val()
    };
    $.ajax({
      type: "POST",
      url: "login",
      data: {
        data: JSON.stringify(user)
      },
      success: function() {
        location.href = "./";
      },
      error: function(jqXHR) {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  },
  logout: function() {
    $.ajax({
      type: "POST",
      url: "logout",
      success: function() {
        location.href = "./";
      },
      error: function(jqXHR) {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  },
  modPizza: function(id) {
    var pizza = {
      nome: id,
      ingredienti: $("#" + id + " .ingredienti").val(),
      prezzo: $("#" + id + " .prezzo").val()
    };
    if(priceRegex.test(pizza.prezzo)) {
      $.ajax({
        type: "POST",
        url: "api/pizza",
        data: {
          data: JSON.stringify(pizza)
        },
        success: function() {
          BootstrapDialog.show({
            title: "Successo",
            message: pizza.nome + " modificata",
            type: BootstrapDialog.TYPE_SUCCESS
          });
          $("#result").html(pizza.nome + " modificata");
        },
        error: function(jqXHR) {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
        }
      });
    } else {
      console.log("Prezzo errato");
    }
  },
  elPizza: function(id) {
    var pizza = {
      nome: id,
      ingredienti: $("#" + id + " .ingredienti").val(),
      prezzo: $("#" + id + " .prezzo").val()
    };
    $.ajax({
      type: "DELETE",
      contentType: "application/json;charset=UTF-8",
      url: "api/pizza",
      data: JSON.stringify(pizza),
      success: function() {
          BootstrapDialog.show({
            title: "Successo",
            message: pizza.nome + " eliminata",
            type: BootstrapDialog.TYPE_SUCCESS
          });
        $("#" + id).remove();
      },
      error: function(jqXHR) {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  },
  insPren: function(e) {
    e.preventDefault();
    var prenotazione = {
      consegna: false,
      orario: $("#orario").val(),
      pizze: {}
    };
    var nomi = [];
    var quant = [];
    $("#prenotazione .nome").each(function() {
      nomi.push($(this).val());
    });
    $("#prenotazione .quantita").each(function() {
      quant.push($(this).val());
    });
    for (i = 0; i<nomi.length; i++) {
      prenotazione.pizze[nomi[i]] = quant[i];
    }
    $.ajax({
      type: "PUT",
      contentType: "application/json;charsetUTF-8",
      url: "api/prenotazione",
      data: JSON.stringify(prenotazione),
      success: function() {
        location.href = "Controller?action=Tue+prenotazioni";
      },
      error: function(jqXHR) {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  },
  modPren: function(id) {
    var prenotazione = {
      id: id,
      consegna: true
    };
    $.ajax({
      type: "POST",
      url: "api/prenotazione",
      data: {
        data: JSON.stringify(prenotazione)
      },
      success: function() {
          BootstrapDialog.show({
            title: "Successo",
            message: "Consegna confermata",
            type: BootstrapDialog.TYPE_SUCCESS
          });
        location.reload();
      },
      error: function() {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  },
  elPren: function(id) {
    var prenotazione = {
      id: id
    };
    $.ajax({
      type: "DELETE",
      contentType: "application/json;charset=UTF-8",
      url: "api/prenotazione",
      data: JSON.stringify(prenotazione),
      success: function() {
          BootstrapDialog.show({
            title: "Successo",
            message: "Prenotazione eliminata",
            type: BootstrapDialog.TYPE_SUCCESS
          });
        location.reload();
      },
      error: function() {
          BootstrapDialog.show({
            title: "Errore",
            message: jqXHR.responseJSON.result,
            type: BootstrapDialog.TYPE_DANGER
          });
      }
    });
  }
};

$(document).ready(pizzFunc.init);