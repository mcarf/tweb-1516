<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Errore!</strong> ${err}
</div>
<p><a href="./" class="btn btn-default">Home</a></p>
