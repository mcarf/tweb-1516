<%--
    Document   : mod_pizza
    Created on : Feb 18, 2016, 2:40:50 AM
    Author     : Matteo Carfora
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-8 col-md-offset-2">
  <h2>Catalogo pizze</h2>
  <form>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Ingredienti</th>
          <th>Prezzo</th>
        </tr>
      </thead>
      <c:forEach var="pizza" items="${pizze}">
        <tr id="${pizza.nome}">
          <td>${pizza.nome}</td>
          <td><input class="form-control ingredienti" type="text" value="${pizza.ingredienti}" /></td>
          <td><input class="form-control prezzo" type="text" value="${pizza.prezzo}"/></td>
          <td>
            <div class="btn-group">
              <button type="button" onclick="pizzFunc.modPizza('${pizza.nome}')" class="btn btn-default">Modifica</button>
              <button type="button" onclick="pizzFunc.elPizza('${pizza.nome}')" class="btn btn-danger">Elimina</button>
            </div>
          </td>
        </tr>
      </c:forEach>
    </table>
  </form>
</div>