<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="col-md-4 col-md-offset-4">
  <form name="input" action="Controller" method="POST">
    <div class="form-group">
      <label for="nome">Nome Pizza</label>
      <input name="nome" id="nome" class="form-control" type="text" value="${ins.nome}" required />
    </div>
    <div class="form-group">
      <label for="ingredienti">Ingredienti</label>
      <input name="ingredienti" id="ingredienti" class="form-control" type="text" value="${ins.ingredienti}" required />
    </div>
    <div class="form-group">
      <label for="prezzo">Prezzo</label>
      <input name="prezzo" id="prezzo" class="form-control" type="number" min="0" step="0.01" value="${ins.prezzo}" id="prezzo" required />
    </div>
    <input name="action" class="btn btn-default" type="submit" value="Inserisci" />
  </form>
</div>
<p>${err}</p>
