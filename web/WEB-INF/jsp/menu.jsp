<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand">Da Pippo</a>
    </div>
    <div class="navbar-collapse collapse">
      <c:choose>
        <c:when test="${empty user}">
          <ul class="nav navbar-nav">
            <li>
              <div class="btn-group navbar-btn">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#login-modal">Login</button>
              </div>
            </li>
          </ul>
        </c:when>
        <c:otherwise>
          <a href="./" class="btn btn-default navbar-btn navbar-left">Catalogo</a>
          <form action="Controller" method="GET" class="navbar-form navbar-left">
            <c:if test="${user.admin}">
              <div class="btn-group">
                <input type="submit" name="action" value="Inserisci pizza" class="btn btn-default" />
                <input type="submit" name="action" value="Modifica pizze" class="btn btn-default" />
                <input type="submit" name="action" value="Vedi prenotazioni" class="btn btn-default" />
              </div>
            </c:if>
            <div class="btn-group">
              <input type="submit" name="action" value="Tue prenotazioni" class="btn btn-default" />
              <input type="submit" name="action" value="Prenota" class="btn btn-default" />
            </div>
          </form>
          <div class="navbar-right">
            <p class="navbar-text">Utente: ${user.id}</p>
            <button id="logout" type="button" class="btn btn-primary navbar-btn">Logout</button>
          </div>
        </c:otherwise>
      </c:choose>
    </div>
  </div>
</nav>
<c:choose>
  <c:when test="${empty user}">
    <div class="modal fade" tabindex="-1" id="login-modal" role="dialog" aria-labelledby="LoginModal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Login</h4>
          </div>
          <form id="login">
            <div class="modal-body">
                <div class="form-group">
                  <label for="user" class="control-label">Username</label>
                  <input type="text" class="form-control" id="user" />
                </div>
                <div class="form-group">
                  <label for="password" class="control-label">Password</label>
                  <input type="password" class="form-control" id="password" />
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
              <input type="submit" class="btn btn-primary" value="Invia"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </c:when>
</c:choose>