<%--
    Document   : prenota
    Created on : Dec 27, 2013, 11:33:03 AM
    Author     : teo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-8 col-md-offset-2">
  <h2>Catalogo pizze</h2>
  <form id="prenotazione">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Ingredienti</th>
          <th>Prezzo</th>
          <th>Quantità</th>
        </tr>
      </thead>
      <c:forEach var="pizza" items="${pizze}">
        <tr>
          <td>${pizza.nome}</td><input class="nome" type="hidden" value="${pizza.nome}"/>
          <td>${pizza.ingredienti}</td>
          <td>${pizza.prezzo}&euro;</td>
          <td><input class="quantita" type="number" min="0" /></td>
        </tr>
      </c:forEach>
    </table>
    <div class="col-md-4 col-md-offset-4">
      <div class="form-group">
        <label for="orario">Quando? (aaaa/mm/gg oo:mm)</label>
        <input id="orario" class="form-control" type="text" placeholder="aaaa/mm/gg oo:mm" required>
      </div>
      <div class="form-group pull-right">
        <button type="submit" class="btn btn-primary">Conferma</button>
      </div>
    </div>
  </form>
</div>