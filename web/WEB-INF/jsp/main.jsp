<%--
   Document   : index
   Created on : 29-ott-2013, 14.39.07
   Author     : st116776
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <title>Pizzeria</title>
  </head>
  <body>
    <jsp:include page="menu.jsp" />
    <div class="container-fluid">
      <div class="page-header">
        <h1 class="text-center">Pizzeria "Da Pippo"</h1>
      </div>
      <div id="content">
        <c:if test="${!empty result}">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p id="result">${result}</p>
          </div>
        </c:if>
        <jsp:include page="${content}" />
      </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-dialog.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
