<%--
    Document   : vis_prenotazioni
    Created on : Feb 18, 2016, 1:52:10 AM
    Author     : Matteo Carfora
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach var="prenotazione" items="${prenotazioni}">
  <div class="col-md-4 col-md-offset-4">
    <table class="table table-striped table-bordered">
      <c:forEach var="pizza" items="${prenotazione.pizze}">
        <tr>
          <td>${pizza.key}</td>
          <td>${pizza.value}</td>
        </tr>
      </c:forEach>
        <tr>
          <td>Consegnata</td>
          <td>
            <c:choose>
              <c:when test="${prenotazione.consegna}">Sì</c:when>
              <c:otherwise>No</c:otherwise>
            </c:choose>
          </td>
        </tr>
        <tr>
          <td>Utente</td>
          <td>${prenotazione.user}</td>
        </tr>
        <tr>
          <td>Orario</td>
          <td>${prenotazione.orario}</td>
        </tr>
    </table>
  </div>
</c:forEach>

