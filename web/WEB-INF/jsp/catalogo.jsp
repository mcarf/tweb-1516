<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-8 col-md-offset-2">
  <h2>Catalogo pizze</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Nome</th>
        <th>Ingredienti</th>
        <th>Prezzo</th>
      </tr>
    </thead>
    <c:forEach var="pizza" items="${pizze}">
      <tr>
        <td>${pizza.nome}</td>
        <td>${pizza.ingredienti}</td>
        <td>${pizza.prezzo}&euro;</td>
      </tr>
    </c:forEach>
  </table>
</div>