<%--
    Document   : prenotazioni
    Created on : Jan 3, 2014, 12:33:02 AM
    Author     : teo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach var="prenotazione" items="${prenotazioni}">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <table class="table table-striped table-bordered">
        <c:forEach var="pizza" items="${prenotazione.pizze}">
          <tr>
            <td>${pizza.key}</td>
            <td>${pizza.value}</td>
          </tr>
        </c:forEach>
        <tr>
          <td>Consegnata</td>
          <td>
            <c:choose>
              <c:when test="${prenotazione.consegna}">Sì</c:when>
              <c:otherwise>No</c:otherwise>
            </c:choose>
          </td>
        </tr>
        <tr>
          <td>Orario</td>
          <td>${prenotazione.orario}</td>
        </tr>
        <c:choose>
          <c:when test="${not prenotazione.consegna}">
            <tr>
              <td><button onclick="pizzFunc.elPren('${prenotazione.id}')" class="btn btn-danger center-block">Elimina</button></td>
              <td><button onclick="pizzFunc.modPren('${prenotazione.id}')" class="btn btn-primary center-block">Consegnata</button></td>
            </tr>
          </c:when>
        </c:choose> 
      </table>
    </div>
  </div>
</c:forEach>

