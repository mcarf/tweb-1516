package db;

import beans.Pizza;
import java.util.List;
import java.util.ArrayList;
import java.sql.*;

public class PizzaDAO extends GenericDAO<Pizza,String> {
    
    @Override
    public String create(Pizza t) {
        try (
                Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement("INSERT INTO PIZZA(NOME,INGREDIENTI,PREZZO)\n"
                        + "VALUES (?,?,?)");
        ) {
            st.setString(1,t.getNome());
            st.setString(2,t.getIngredienti());
            st.setFloat(3,t.getPrezzo());
            st.executeUpdate();
        } catch(SQLException e) { return ""; }
        return t.getNome();
    }
    
    public Pizza read(String pk) {
        Pizza t = null;
        try (
                Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement("SELECT * FROM PIZZA WHERE NOME = ?");
        ) {
            st.setString(1,pk);
            ResultSet rs = st.executeQuery();
            if(rs.next()) {
                t = new Pizza();
                t.setNome(rs.getString("NOME"));
                t.setIngredienti(rs.getString("INGREDIENTI"));
                t.setPrezzo(rs.getFloat("PREZZO"));
            }
        } catch(SQLException e) { return null; }
        return t;
    }
    
    public boolean update(Pizza t) {
        try (
                Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement("UPDATE PIZZA SET INGREDIENTI = ?, PREZZO = ?\n"
                        + "WHERE NOME = ?");
        ) {
            st.setString(1,t.getIngredienti());
            st.setFloat(2,t.getPrezzo());
            st.setString(3,t.getNome());
            st.executeUpdate();
        } catch(SQLException e) { return false; }
        return true;
    }
    
    public boolean delete(Pizza t) {
        try (
                Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement("DELETE FROM PIZZA WHERE NOME = ?");
        ) {
            st.setString(1,t.getNome());
            st.executeUpdate();
        } catch(SQLException e) { return false; }
        return true;
    }
    
    public List<Pizza> listAll() {
        ArrayList<Pizza> pizze = new ArrayList<>();
        try (
                Connection conn = ds.getConnection();
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM PIZZA");
        ) {
            while (rs.next()) {
                Pizza pizza = new Pizza();
                pizza.setNome(rs.getString("NOME"));
                pizza.setIngredienti(rs.getString("INGREDIENTI"));
                pizza.setPrezzo(rs.getFloat("PREZZO"));
                pizze.add(pizza);
            }
        } catch(SQLException e) { e.printStackTrace(); }
        return pizze;
    }
}