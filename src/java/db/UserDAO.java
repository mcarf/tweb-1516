package db;

import beans.User;
import java.sql.*;
import javax.servlet.*;

public class UserDAO extends GenericDAO<User,String> {
    
    @Override
    public String create(User t) {
        try (
                Connection conn = ds.getConnection();
                PreparedStatement st = conn.prepareStatement("INSERT INTO UTENTE(ID_ACC,PASS,ADMIN)"
                        + "VALUES(?,?,?)");
        ) {
            st.setString(1,t.getId());
            st.setString(2,t.getPassword());
            st.setBoolean(3,t.isAdmin());
            st.executeUpdate();
        } catch(SQLException e) { return ""; }
        return t.getId();
    }

    public User read(String pk) {
        User t = null;
        try (
            Connection conn = ds.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM UTENTE WHERE ID_ACC = ?");
        ) {
            st.setString(1,pk);
            ResultSet rs = st.executeQuery();
            if(rs.next()) {
                t = new User();
                t.setId(rs.getString("ID_ACC"));
                t.setPassword(rs.getString("PASS"));
                t.setAdmin(rs.getBoolean("ADMIN"));
            }
        } catch(SQLException e) { return null; }
        return t;
    }
}
