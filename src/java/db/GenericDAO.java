package db;

import org.apache.derby.jdbc.ClientDataSource;

public abstract class GenericDAO <T,PK> {
    protected ClientDataSource ds;
    
    public GenericDAO() {
        ds = new org.apache.derby.jdbc.ClientDataSource();
        ds.setDatabaseName("sample");
        ds.setUser("app");
        ds.setPassword("app");
        ds.setServerName("localhost");
        ds.setPortNumber(1527);
    }
    
    abstract PK create(T newInstance);
}
