package db;

import beans.Prenotazione;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class PrenotazioneDAO extends GenericDAO<Prenotazione, Integer> {

  private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

  @Override
  public Integer create(Prenotazione t) {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
    } catch (SQLException e) {
      return null;
    }
    try (
            PreparedStatement st1 = conn.prepareStatement("INSERT INTO PRENOTAZIONE(UTENTE,ORARIO,CONSEGNA)"
                    + "VALUES(?,?,?)", new String[]{"ID_PR"});
            PreparedStatement st2 = conn.prepareStatement("INSERT INTO QUANT(PR,PIZZA,QUANT) VALUES(?,?,?)");) {
      st1.setString(1, t.getUser());
      Timestamp ts = new Timestamp(sdf.parse(t.getOrario()).getTime());
      st1.setTimestamp(2, ts);
      st1.setBoolean(3, t.isConsegna());
      st1.executeUpdate();

      ResultSet rs = st1.getGeneratedKeys();
      rs.next();
      int id = rs.getInt(1);
      rs.close();

      for (String s : t.getPizze().keySet()) {
        st2.setInt(1, id);
        st2.setString(2, s);
        st2.setInt(3, t.getPizze().get(s));
        st2.addBatch();
      }
      st2.executeBatch();
      conn.commit();
    } catch (SQLException | ParseException e) {
      try {
        conn.rollback();
      } catch (SQLException ex) {
      }
      return null;
    } finally {
      try {
        conn.close();
      } catch (SQLException e) {
      }
    }
    return t.getId();
  }

  public Prenotazione read(int pk) {
    Prenotazione t = null;
    try (
            Connection conn = ds.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM PRENOTAZIONE WHERE ID_PR = ?")
    ) {
      st.setInt(1,pk);
      ResultSet rs = st.executeQuery();
      if(rs.next()) {
        t = new Prenotazione();
        t.setId(rs.getInt("ID_PR"));
        t.setUser(rs.getString("UTENTE"));
        Date d = new Date(rs.getTimestamp("ORARIO").getTime());
        t.setOrario(sdf.format(d));
        t.setConsegna(rs.getBoolean("CONSEGNA"));
      }
    } catch(SQLException e) {
      return null;
    }
    return t;
  }

  public boolean delete(Prenotazione t) {
    try (
            Connection conn = ds.getConnection();
            PreparedStatement st1 = conn.prepareStatement("DELETE FROM QUANT WHERE PR = ?");
            PreparedStatement st2 = conn.prepareStatement("DELETE FROM PRENOTAZIONE WHERE ID_PR = ?");) {
      st1.setInt(1, t.getId());
      st1.executeUpdate();
      st2.setInt(1, t.getId());
      st2.executeUpdate();
    } catch (SQLException e) {
      return false;
    }
    return true;
  }

  public boolean setConsegna(Prenotazione t) {
    try (
            Connection conn = ds.getConnection();
            PreparedStatement st = conn.prepareStatement("UPDATE PRENOTAZIONE SET CONSEGNA = ?\n"
                    + "WHERE ID_PR = ?");) {
      st.setBoolean(1, t.isConsegna());
      st.setInt(2, t.getId());
      st.executeUpdate();
    } catch (SQLException e) {
      return false;
    }
    return true;
  }

  public List<Prenotazione> listByUser(String user) {
    ArrayList<Prenotazione> pren;
    try (
            Connection conn = ds.getConnection();
            PreparedStatement st1 = conn.prepareStatement("SELECT * FROM PRENOTAZIONE WHERE UTENTE = ?");
            PreparedStatement st2 = conn.prepareStatement("SELECT * FROM QUANT WHERE PR = ?");) {
      st1.setString(1, user);
      ResultSet rs1 = st1.executeQuery();
      pren = new ArrayList<>();
      while (rs1.next()) {
        Prenotazione p = new Prenotazione();
        p.setId(rs1.getInt("ID_PR"));
        p.setUser(user);
        Date d = new Date(rs1.getTimestamp("ORARIO").getTime());
        p.setOrario(sdf.format(d));
        p.setConsegna(rs1.getBoolean("CONSEGNA"));

        st2.setInt(1, p.getId());
        ResultSet rs2 = st2.executeQuery();
        HashMap<String, Integer> pizze = new HashMap<>();
        while (rs2.next()) {
          pizze.put(rs2.getString("PIZZA"), rs2.getInt("QUANT"));
        }
        p.setPizze(pizze);

        pren.add(p);
      }

    } catch (SQLException e) {
      return null;
    }
    return pren;
  }

  public List<Prenotazione> listAll() {
    ArrayList<Prenotazione> pren;
    try (
            Connection conn = ds.getConnection();
            PreparedStatement st1 = conn.prepareStatement("SELECT * FROM PRENOTAZIONE");
            PreparedStatement st2 = conn.prepareStatement("SELECT * FROM QUANT WHERE PR = ?");) {
      ResultSet rs1 = st1.executeQuery();
      pren = new ArrayList<>();
      while (rs1.next()) {
        Prenotazione p = new Prenotazione();
        p.setId(rs1.getInt("ID_PR"));
        p.setUser(rs1.getString("UTENTE"));
        Date d = new Date(rs1.getTimestamp("ORARIO").getTime());
        p.setOrario(sdf.format(d));
        p.setConsegna(rs1.getBoolean("CONSEGNA"));

        st2.setInt(1, p.getId());
        ResultSet rs2 = st2.executeQuery();
        HashMap<String, Integer> pizze = new HashMap<>();
        while (rs2.next()) {
          pizze.put(rs2.getString("PIZZA"), rs2.getInt("QUANT"));
        }
        p.setPizze(pizze);

        pren.add(p);
      }

    } catch (SQLException e) {
      return null;
    }
    return pren;
  }
}
