package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import beans.*;
import db.*;

/**
 *
 * @author st116776
 */
@WebServlet(urlPatterns = "/Controller", name = "Controller")
public class Controller extends HttpServlet {
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletContext ctx = getServletContext();
        response.setContentType("text/html;charset=UTF-8");

        String op = request.getParameter("action");
        if (op == null) {
            op = "home";
        }
        User user = (User)request.getSession().getAttribute("user");
        switch (op.toLowerCase()) {
            case "home":
                request.setAttribute("pizze", new PizzaDAO().listAll());
                request.setAttribute("content","catalogo.jsp");
                break;
            case "registrati":
                request.setAttribute("content","reg_user.jsp");
                break;
            case "inserisci pizza":
                if(user != null && user.isAdmin()) {
                    request.setAttribute("content","ins_pizza.jsp");
                } else {
                    request.setAttribute("err","Amministratore non autenticato!");
                    request.setAttribute("content","error.jsp");
                }
                break;
            case "modifica pizze":
              if(user != null && user.isAdmin()) {
                request.setAttribute("pizze", new PizzaDAO().listAll());
                request.setAttribute("content", "mod_pizza.jsp");
              } else {
                request.setAttribute("err","Amministratore non autenticato!");
                request.setAttribute("content","error.jsp");
              }
              break;
            case "vedi prenotazioni":
              if(user != null && user.isAdmin()) {
                request.setAttribute("prenotazioni", new PrenotazioneDAO().listAll());
                request.setAttribute("content","vis_prenotazioni.jsp");
              } else {
                request.setAttribute("err","Amministratore non autenticato!");
                request.setAttribute("content", "error.jsp");
              }
              break;
            case "tue prenotazioni":
                if(user != null) {
                    request.setAttribute("prenotazioni", new PrenotazioneDAO().listByUser(user.getId()));
                    request.setAttribute("content","prenotazioni.jsp");
                    ctx.log("ciao");
                } else {
                    request.setAttribute("err","Utente non autenticato!");
                    request.setAttribute("content","error.jsp");
                }
                break;
            case "prenota":
              if(user != null) {
                request.setAttribute("pizze", new PizzaDAO().listAll());
                request.setAttribute("content", "prenota.jsp");
              } else {
                request.setAttribute("err", "Utente non autenticato!");
                request.setAttribute("content","error.jsp");
              }
              break;
            case "registra":
                ctx.getNamedDispatcher("Insert").include(request,response);
                break;
            case "inserisci":
                if(user != null && user.isAdmin()) {
                    ctx.getNamedDispatcher("Insert").include(request,response);
                } else {
                    request.setAttribute("err","Amministratore non autenticato!");
                    request.setAttribute("content","error.jsp");
                }
                break;
            default:
                request.setAttribute("err", "Operazione non possibile!");
                request.setAttribute("content", "error.jsp");
        }
        ctx.getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(request,response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
