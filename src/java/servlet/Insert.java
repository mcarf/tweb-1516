package servlet;

import db.UserDAO;
import db.PizzaDAO;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import beans.*;

/**
 *
 * @author st116776
 */
@WebServlet(urlPatterns="/Insert", name="Insert")
public class Insert extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletContext ctx = getServletContext();
        response.setContentType("text/html;charset=UTF-8");


        String op = request.getParameter("action");
        if(op == null) op = "err";
        switch(op.toLowerCase()) {
            case "registra":
                User user = new User();
                user.setId(request.getParameter("user"));
                user.setPassword(request.getParameter("pass"));
                user.setAdmin(false);
                if(!new UserDAO().create(user).equals(user.getId())) {
                    request.setAttribute("content","/reg_user.jsp");
                    request.setAttribute("ins", user);
                    request.setAttribute("err","Username già utilizzato(?)");
                } else {
                    request.setAttribute("result","Registrazione effettuata");
                    request.getSession().setAttribute("user",user);
                }
                break;
            case "inserisci":
                Pizza pizza = new Pizza();
                pizza.setNome(request.getParameter("nome"));
                pizza.setIngredienti(request.getParameter("ingredienti"));
                pizza.setPrezzo(Float.parseFloat(request.getParameter("prezzo")));
                if(!new PizzaDAO().create(pizza).equals(pizza.getNome())) {
                    request.setAttribute("content","/ins_pizza.jsp");
                    request.setAttribute("ins", pizza);
                    request.setAttribute("err","Pizza già presente(?)");
                } else {
                    request.setAttribute("result","Pizza inserita");
                }
                break;
            default:
                request.setAttribute("content","/error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
