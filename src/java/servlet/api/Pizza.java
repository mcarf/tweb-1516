package servlet.api;

import beans.JSONData;
import beans.User;
import com.google.gson.Gson;
import db.PizzaDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Matteo Carfora
 */
@WebServlet(name = "APIPizza", urlPatterns = {"/api/pizza"})
public class Pizza extends HttpServlet {

  private final PizzaDAO pDAO = new PizzaDAO();

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    HttpSession session = request.getSession();
    Gson gson = new Gson();

    try (PrintWriter out = response.getWriter()) {
      User u = (User) session.getAttribute("user");
      beans.Pizza p = gson.fromJson(request.getParameter("data"), beans.Pizza.class);
      beans.Pizza v = pDAO.read(p.getNome());
      if (u != null && u.isAdmin()) {
        pDAO.update(p);
        response.setStatus(200);
        out.write(gson.toJson(new JSONData("OK")));
      }
      else {
        response.setStatus(400);
        out.write(gson.toJson(new JSONData("Pizza non modificabile")));
      }
    } catch (Exception e) {
      response.sendError(400);
    }
  }

  /**
   * Handles the HTTP <code>PUT</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPut(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    HttpSession session = request.getSession();
    Gson gson = new Gson();

    try (PrintWriter out = response.getWriter()) {
      User u = (User) session.getAttribute("user");
      BufferedReader reader =
              new BufferedReader(new InputStreamReader(request.getInputStream()));
      beans.Pizza p = gson.fromJson(reader, beans.Pizza.class);
      if (u != null && u.isAdmin()) {
        pDAO.create(p);
        response.setStatus(200);
        out.write(gson.toJson(new JSONData("OK")));
        System.out.println(p);
      }
      else {
        response.setStatus(400);
        out.write(gson.toJson(new JSONData("Non autenticato")));
      }
    } catch (Exception e) {
      response.sendError(400);
    }
  }

  /**
   * Handles the HTTP <code>DELETE</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doDelete(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    HttpSession session = request.getSession();
    Gson gson = new Gson();

    try (PrintWriter out = response.getWriter()) {
      User u = (User) session.getAttribute("user");
      BufferedReader reader =
              new BufferedReader(new InputStreamReader(request.getInputStream()));
      beans.Pizza p = gson.fromJson(reader, beans.Pizza.class);
      if (u != null && u.isAdmin() && pDAO.delete(p)) {
        response.setStatus(200);
        out.write(gson.toJson(new JSONData("OK")));
      }
      else {
        response.setStatus(400);
        out.write(gson.toJson(new JSONData("Pizza non cancellabile")));
      }
    } catch (Exception e) {
      response.sendError(400);
    }
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }
}
