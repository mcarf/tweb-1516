package servlet.api;

import beans.User;
import com.google.gson.Gson;
import db.PrenotazioneDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Matteo Carfora
 */
@WebServlet(name = "APIPrenotazioni", urlPatterns = {"/api/prenotazioni"})
public class Prenotazioni extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    HttpSession session = request.getSession();
    Gson gson = new Gson();
    try (PrintWriter out = response.getWriter()) {
      User u = (User)session.getAttribute("user");
      if(u != null) {
        out.write(gson.toJson(new PrenotazioneDAO().listByUser(u.getId())));
      }
    }
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }

}
