package servlet;

import beans.JSONData;
import beans.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import db.UserDAO;
import java.io.PrintWriter;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Matteo Carfora
 */
@WebServlet(name = "Login", urlPatterns = {"/login"})
public class Login extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("application/json;charset=UTF-8");
    HttpSession session = request.getSession();
    Gson gson = new Gson();
    User u;
    try (PrintWriter out = response.getWriter()) {
      u = gson.fromJson(request.getParameter("data"), User.class);
      User v = new UserDAO().read(u.getId());
      if (v != null && v.getPassword().equals(u.getPassword())) {
        session.setAttribute("user", v);
        response.setStatus(200);
        out.write(gson.toJson(new JSONData("OK")));
      }
      else {
        response.setStatus(400);
        out.write(gson.toJson(new JSONData("User o password invalidi")));
      }

    } catch(JsonSyntaxException e) {
      response.sendError(400);
    }
  }


  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}