package beans;

public class JSONData {
  private String result;

  public JSONData(String result) {
    this.result = result;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }
}
