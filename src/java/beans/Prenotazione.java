package beans;

import java.util.HashMap;


public class Prenotazione {
    private int id;
    private String user;
    private String orario;
    private boolean consegna;
    private HashMap<String,Integer> pizze;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    public String getOrario() {
        return orario;
    }
    public void setOrario(String orario) {
        this.orario = orario;
    }

    public boolean isConsegna() {
        return consegna;
    }
    public void setConsegna(boolean consegna) {
        this.consegna = consegna;
    }

    public HashMap<String, Integer> getPizze() {
        return pizze;
    }
    public void setPizze(HashMap<String, Integer> pizze) {
        this.pizze = pizze;
    }
}
