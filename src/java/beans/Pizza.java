package beans;

public class Pizza {

    private String nome;
    private String ingredienti;
    private float prezzo;
    
    public Pizza() {}
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getIngredienti() {
        return ingredienti;
    }
    
    public void setIngredienti(String ingredienti) {
        this.ingredienti = ingredienti;
    }
    
    public float getPrezzo() {
        return prezzo;
    }
    
    public void setPrezzo(float value) {
        prezzo = value;
    }
}
